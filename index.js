'use strict';

/*
 * @api public
 * @property {function} format
 * Both the construction method and set of exposed
 * formats.
 */
const format = exports.format = require('./format');

/*
 * @api public
 * @method {function} levels
 * Registers the specified levels with logform.
 */
exports.levels = require('./levels');

/*
 * @api private
 * method {function} exposeFormat
 * Exposes a sub-format on the main format object
 * as a lazy-loaded getter.
 */
function exposeFormat(name, path) {
  path = path || name;
  Object.defineProperty(format, name, {
    get() {
      return require(`./${path}.js`);
    },
    configurable: true
  });
}

const align = require('./align');
const errors = require('./errors');
const cli = require('./cli');
const combine = require('./combine');
const colorize = require('./colorize');
const json = require('./json');
const label = require('./label');
const logstash = require('./logstash');
const metadata = require('./metadata');
const ms = require('./ms');
const padLevels = require('./pad-levels');
const prettyPrint = require('./pretty-print');
const printf = require('./printf');
const simple = require('./simple');
const splat = require('./splat');
const timestamp = require('./timestamp');
const uncolorize = require('./uncolorize');
// help to recognize dynamic import
exports.format = {
  ...format,
  align,
  errors,
  cli,
  combine,
  colorize,
  json,
  label,
  logstash,
  metadata,
  ms,
  padLevels,
  prettyPrint,
  printf,
  simple,
  splat,
  timestamp,
  uncolorize
};
//
// Setup all transports as lazy-loaded getters.
//
exposeFormat('align');
exposeFormat('errors');
exposeFormat('cli');
exposeFormat('combine');
exposeFormat('colorize');
exposeFormat('json');
exposeFormat('label');
exposeFormat('logstash');
exposeFormat('metadata');
exposeFormat('ms');
exposeFormat('padLevels', 'pad-levels');
exposeFormat('prettyPrint', 'pretty-print');
exposeFormat('printf');
exposeFormat('simple');
exposeFormat('splat');
exposeFormat('timestamp');
exposeFormat('uncolorize');
